
import pyrebase #A PYTHON WRAPPER FOR FIREBASE API
import tkinter as tk #FOR CREATING THE GUI
import InterfaceMain as im #IT REFERS TO THE FILE WITH THE APPLICATION'S MAIN FEATURES


app = tk.Tk() #INITIALIZING THE APPLICATION OBJECT
app.title("Emotion Recognition Application") #SETTING THE WINDOW'S TITLE
app.geometry('400x240') #SETTING THE WINDOW'S DIMENSIONS

firebaseConfig = {
  'apiKey': "AIzaSyCyJtZ3pUWB-wy6adKafBqhdSMQC-TCM4I",
  'authDomain': "emotionmonitoringapp.firebaseapp.com",
  'databaseURL': "https://emotionmonitoringapp-default-rtdb.firebaseio.com/",
  'projectId': "emotionmonitoringapp",
  'storageBucket': "emotionmonitoringapp.appspot.com",
  'messagingSenderId': "263130720273",
  'appId': "1:263130720273:web:cc61cc48d454050b56a9b0",
  'measurementId': "G-1C47339J26"
} #A DICTIONARY STORING THE CONFIGURATION SPECIFICATIONS USED TO CONNECT TO THE FIREBASE PROJECT

firebase = pyrebase.initialize_app(firebaseConfig) #INITIALIZING THE FIREBASE APPLICATION USING THE firebaseConfig DICTIONARY
auth = firebase.auth() #CREATING AN auth MODULE'S INSTANCE FOR AUTHENTICATING

def login_task(): #DEFINING THE login_task FUNCTION
    email = username_entry.get() #RETRIEVING THE USERNAME FROM THE username_entry WIDGET WITH THE get() FUNCTION AND ASSIGNING IT TO email
    password = password_entry.get() #RETRIEVING THE PASSWORD FROM THE password_entry WIDGET WITH THE get() FUNCTION AND ASSIGNING IT TO password
    try: #FOR ERROR MANAGEMENT
        Login = auth.sign_in_with_email_and_password(email, password) #SIGNING IN TO FIREBASE
        app.withdraw()  #HIDING THE AUTHENTICATION WINDOW
        im.startInterface(app, email, password)  #CALLING THE startInterface() FUNCTION, PASSING AS ARGUMENTS THE APPLICATION WINDOW,
                                                 # THE GMAIL ADDRESS AND THE PASSWORD OF THE LOGGED IN USER
    except: #IF IT CANNOT LOGIN WITH THE ENTERED CREDENTIALS
        print("not logged in") #PRINT "not logged in" IN TERMINAL

def signup_task(): #DEFINING THE signup_task FUNCTION
    email = username_entry.get() #RETRIEVING THE USERNAME FROM THE username_entry WIDGET WITH THE get() FUNCTION AND ASSIGNING IT TO email
    password = password_entry.get() #RETRIEVING THE PASSWORD FROM THE password_entry WIDGET WITH THE get() FUNCTION AND ASSIGNING IT TO password
    try: #FOR ERROR MANAGEMENT
        user = auth.create_user_with_email_and_password(email, password) #CREATING A NEW USER IN FIREBASE
        print(email) #IF SUCCESSFUL, PRINTING IN THE TERMINAL THE MAIL
    except: #IF IT COULD NOT REGISTER THE USER WITH THE ENTERED MAIL AND PASSWORD
        print("not signed up, try a longer password") #PRINT "not signed up, try a longer password" IN THE TERMINAL

username_label = tk.Label(app, #tk.Label IS USED FOR CREATING A LABEL WIDGET DESPLAYED IN THE app WIDGET
                          text="Username", #text PARAMETER FOR THE LABEL'S TEXT
                          width=80, #THE LABEL'S WIDTH
                          height=3, #THE LABEL'S HEIGHT
                          fg="black") #THE LABEL'S FOREGROUND

username_label.place(relx=0.2, rely=0.3, anchor=tk.CENTER) #THE place METHOD POSITIONS THE WIDGET
                                                           #THE LABEL BEING PLACED AT APROXIMATELY 30% OF THE WIDTH
                                                           #AND 30% OF THE HEIGHT OF THE WINDOW
                                                           #THE REFERENCE POINT OF THE WINDOW IS tk.CENTER
password_label = tk.Label(app,
                          text="Password",
                          width=80,
                          height=3,
                          fg="black")

password_label.place(relx=0.2, rely=0.5, anchor=tk.CENTER)

username_entry = tk.Entry(app, #tk.Entry IS USED FOR CREATING AN ENTRY FIELD WIDGET,
                               #GRANTING USERS THE ABILITY TO INPUT TEXT
                          width=30)
username_entry.place(relx=0.65, rely=0.3, anchor=tk.CENTER)

password_entry = tk.Entry(app,
                          width=30,
                          show="*") #THE PASSWORD WILL BE WRITTEN AS A STRING OF *S
                                    #IN ORDER TO HIDE THE ENTERED PASSWORD FOR PRIVACY
password_entry.place(relx=0.65, rely=0.5, anchor=tk.CENTER)

login_button = tk.Button(app,
                         width=10,
                         height=2,
                         text="Login",
                         command=login_task) #THE COMMAND PARAMETER IS USED TO SPECIFY THE FUNCTION
                                             #TO BE CALLED WHEN THE BUTTON IS PRESSED
login_button.place(relx=0.79, rely=0.7, anchor=tk.CENTER)

signup_button = tk.Button(app,
                          width=10,
                          height=2,
                          text="SignUp",
                          command=signup_task)
signup_button.place(relx=0.5, rely=0.7, anchor=tk.CENTER)

app.mainloop() #INITIATING THE MAIN EVENT LOOP, USED TO HANDLE ALL USER EVENTS AND TO KEEP THE WINDOW RESPONSIVE


