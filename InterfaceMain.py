
import tkinter as tk
from tkinter import scrolledtext
import pandas as pd
from tidytext import unnest_tokens
import nltk

nltk.download('stopwords')
from nltk.corpus import stopwords
from siuba import *
import numpy as np


from afinn import Afinn
import spacy

import cv2
from deepface import DeepFace
from PIL import Image, ImageTk
import pyrebase

face_cascade_name = cv2.data.haarcascades + 'haarcascade_frontalface_alt.xml'
face_cascade = cv2.CascadeClassifier()
if not face_cascade.load(cv2.samples.findFile(face_cascade_name)):
    print("Error loading XML file")
nltk.download('punkt')  # Download the necessary tokenizer data

count_monitoring = 0 #VARIABLE FOR COUNTING MONITORING IN REAL-TIME (DISPLAYED IN TERMINAL)

firebaseConfig = {
  'apiKey': "AIzaSyCyJtZ3pUWB-wy6adKafBqhdSMQC-TCM4I",
  'authDomain': "emotionmonitoringapp.firebaseapp.com",
  'databaseURL': "https://emotionmonitoringapp-default-rtdb.firebaseio.com/",
  'projectId': "emotionmonitoringapp",
  'storageBucket': "emotionmonitoringapp.appspot.com",
  'messagingSenderId': "263130720273",
  'appId': "1:263130720273:web:cc61cc48d454050b56a9b0",
  'measurementId': "G-1C47339J26"
}

firebase = pyrebase.initialize_app(firebaseConfig)
auth = firebase.auth()

def startInterface(auth_window, username, password):
    #CREATING THE interfaceWindow
    interfaceWindow = tk.Toplevel(auth_window)
    interfaceWindow.title('Emotion Monitoring Application')
    interfaceWindow.geometry("770x600+200+200")

    def logout(): #logout FUNCTION DEFINITION WITH NO ARGUMENTS
        interfaceWindow.destroy() #CALLING THE destroy() METHOD ON THE interfaceWindow OBJECT TO CLOSE THE WINDOW
        auth_window.deiconify() #CALLING THE deiconify() METHOD ON THE auth_window OBJECT TO MAKE IT RETURN TO ITS NORMAL SHOWN STATE

    def sentimentAnalysis(): #sentimentAnalysis FUNCTION DEFINITION WITH NO ARGUMENTS
        inputtextbox = textboxinserttext.get(1.0, "end-1c") #RETRIEVING THE TEXT INPUT FROM THE textboxinserttext WIDGET AND
                                                            #ASSIGNING IT TO THE inputtextbox VARIABLE
        poems_split = inputtextbox.splitlines()  #SPLITTING THE TEXT FROM inputtextbox INTO A LIST OF LINES, USING THE splitlines() METHOD
                                                 #THAT TRANSFORMS EACH LINE OF TEXT FROM THE WIDGET IN AN ELEMENT IN THE poems_split LIST
                                                 #~I CALLED THE TEXTS "POEMS" TO RENDER HONOR TO THE ORIGIN OF MY APPLICATION IDEA,
                                                 #MONITORING PEOPLE'S EMOTIONS WHILE READING A POEM~
        df_poems = pd.DataFrame({
            "content": poems_split
        }) #CREATING A pandas DATAFRAME NAMED df_poems, CONTAINING A SINGLE COLUMN NAMED "content".
           #THE DATA FOR THE COLUMN BEING THE poems_split LIST
        df = unnest_tokens(df_poems.astype(str), "word", "content")
        #USING THE unnest_tokens() FUNCTION FROM THE tidytext MODULE IN ORDER TO TOKENIZE THE TEXT IN THE df_poems DATAFRAME
        #THE PARAMETER "content" SPECIFIES THE COLUMN TO TAKE THE DATA FROM, WHILE "word" THE NAME OF THE NEW COLUMN
        #CREATED IN THE df DATAFRAME
        df.reset_index(drop=True, inplace=True)
        #RESETING THE INDEX OF df DATAFRAME


        basic_stop_words = stopwords.words('english')
        #RETRIEVING A LIST OF BASIC STOP WORDS TAKEN FROM THE NLTK LIBRARY IN ENGLISH
        custom_stop_words = ['a', 'in', 'by', 'at', 'for', 'an', 'of', 'with', 'said', 'one', 'dont', 'thats', 'a.',
                             '\'s',
                             'let', 'us', 'wo', 'ai', "n't", 'had', 'came', 'have', 'was', '', '\n', 'ive', 'oh']
        #ADDING ADDITIONAL STOP WORDS WITH A CUSTOM LIST FOR THIS APPLICATION IN PARTICULAR
        punctuation = ['?', ',', '.', '!', '-', ';', ')', '(', '[', ']', ':', '"', '“', '”', '’', '‘', '``', '--',
                       '\'\'',
                       '\'']
        #ADDING A LIST OF PUNCTUATION MARKS
        basic_stop_words.extend(punctuation)
        basic_stop_words.extend(custom_stop_words)
        #EXTENDING THE basic_stop_words WITH THE ABOVE CREATED ADDITIONAL LISTS
       #IL MAI PUN?? df = df[df.notnull()].reset_index(drop=True) #REMOVING NULL VALUES FROM THE df DATAFRAME
        df = df[~df['word'].isin(basic_stop_words)] #FILTERING df BY REMOVING FROM IT ANY WORD CONTAINED IN basic_stop_words
        df = df[df.word.notnull()].reset_index(drop=True) #REMOVING NULL VALUES THE df DATAFRAME'S "word" COLUMN


        result = count(df, 'word', sort=True).head(22)
        mostusedwords = result
        #CALCULATING THE COUNT OF EVERY UNIQUE WORD IN THE COLUMN "word" AND STORE THE RESULT IN result VARIABLE
        #ASSIGNING THE VALUE OF result TO mostusedwords IN ORDER TO RETURN IT AND USE IT IN THE showResults FUNCTION


        afinn = Afinn() #INITIALIZING AN AFFIN CLASS INSTANCE TO PERFORM SENTIMENT ANALYSIS
        poems_df_words_sentiment = df.copy() #MAKING A COPY OF df DATAFRAME IN ORDER TO AVOID CHANGING THE df DATAFRAME
        poems1_df_words_sentiment = poems_df_words_sentiment #MAKING A REFERENCE TO THE SAME DATAFRAME OBJECT
        word_scores1 = [afinn.score(word) for word in poems1_df_words_sentiment['word']]
        #CALCULATING THE SENTIMENT SCORE FOR EVERY WORD FROM THE word COLUMN, USING THE score() METHOD
        poems1_df_words_sentiment = poems1_df_words_sentiment.assign(word_score=word_scores1)
        #ASSIGNING THE RESULTS IN A NEW "word_score" COLUMN

        mean = poems1_df_words_sentiment["word_score"].mean()
        #COMPUTING THE MEAN OF ALL THE VALUES IN THE word_score COLUMN
        global positivityscoreoftext
        positivityscoreoftext = round(mean, 1)
        #ASSIGNING TO positivityscoreoftext THE VALUE OF mean VARIABLE ROUNDED TO ONE DECIMAL


        maxim = float('-inf') #INITIALIZING THE maxim VAR TO NEGATIVE INFINITY
        minim = float('inf') #INITIALIZING THE minim VAR TO POSITIVE INFINITY
        for index, row in poems1_df_words_sentiment.iterrows(): #FOR LOOP ITERATING THROUGH EACH ROW IN THE poems1_df_words_sentiment DATAFRAME
            if row['word_score'] > maxim:
                maxim = row['word_score']
            #IF FINDING A HIGHER SCORE THAN maxim'S VALUE, maxim IS ASSIGNED WITH IT
            if row['word_score'] < minim:
                minim = row['word_score']
            #IF FINDING A LOWER SCORE THAN minim'S VALUE, minim IS ASSIGNED WITH IT
        positive_words = poems1_df_words_sentiment[poems1_df_words_sentiment['word_score'] == maxim]
        #CREATING A NEW positive_words DATAFRAME CONTAINING ONLY THE ROWS FROM poems1_df_words_sentiment DATAFRAME WHERE word_score HAS A VALUE EQUAL TO maxim
        mostpositivewords = positive_words[['word', 'word_score']]
        #ASSIGNING TO THE VARIABLE THE SUBSET OF COLUMNS WITH THE MOST POSITIVE WORDS
        negative_words = poems1_df_words_sentiment[poems1_df_words_sentiment['word_score'] == minim]
        #CREATING A NEW negative_words DATAFRAME CONTAINING ONLY THE ROWS FROM poems1_df_words_sentiment DATAFRAME WHERE word_score HAS A VALUE EQUAL TO mininm
        mostnegativewords = negative_words[['word', 'word_score']]
        #ASSIGNING TO THE VARIABLE THE SUBSET OF COLUMNS WITH THE MOST NEGATIVE WORDS



        nlp = spacy.load('en_core_web_sm') #LOADING THE ENGLISH LANGUAGE MODEL FROM THE spaCy LIBRARY
        max_length = 1000000 - 1 #THE MAXIMUM NUMBER OF CHARACTERS THAT COULD BE ANALYZED
        poems_to_test = df[:max_length] #SLIICNG THE DATAGRAME df AFTER THE max_length-th CHARACTER
                                        #IN ORDER TO PREVENT ANY PERFORMANCE RELATED ISSUE
        doc = nlp(poems_to_test.to_string()) #PROCESSING THE TEXT IN poems_to_test, USING THE nlp MODEL
                                             #FROM THE spaCy LIBRARY THE ANALYZED DOCUMENT BEING ASSIGNED TO doc OBJECT
        poems_topics = []
        for token in doc:
            if token.pos_ == "VERB":
            #ITERATING THROUGH EVERY TOKEN IN THE doc OBJECT AND CHECKING IF ITS PART-OF-SPEECH TAG token.pos_ IS "VERB"
                if token.text.strip() not in ['had', 'have', 'was', 'do', 'come', 'is']:
                #EXCLUDING SOME USUAL VERBS THAT WOULDN'T AFFECT THE TEXT TOO MUCH MEANING WISE
                    poems_topics.append(token.lemma_.strip())
                    #EXTRACTING THE LEMMA, OR BASE FORM OF SAID VERB FOUND
        df_poems = pd.DataFrame(poems_topics, columns=["Relevant verbs"])
        #CREATING A DATAFRAME FROM THE poems_topics LIST WITH A Relevant verbs COLUMN
        relevantverbs = df_poems
        #ASSIGNING ITS VALUE TO relevantverbs

        return mostusedwords, relevantverbs, mostpositivewords, mostnegativewords, positivityscoreoftext
        #RETURNING THE 5 PARAMETERS TO BE DISPLAYED IN THE Results WINDOW

    def computePositivity(x):
        return 10 * x + 50
        #THE FORMULA FOR COMPUTING POSITIVITY IN PERCENTAGES FROM THE Afinn SCORE,
        #THE Afinn SCORE RANGING ONLY FROM -5 TO +5
        #AS AN EXAMPLE, IF A WORD WOULD HAVE Afinn SCORE 1, THEN ITS PERCENTAGE OF POSITIVITY WOULD BE 60%
        #0 Afinn SCORE CORRESPONDS TO 50%, WHILE +5 TO 100% AND -5 TO 0%

    global name_entry,pos_per,neg_per,pos_ex_per,neg_ex_per,comp
    def save_info(): #FUNCTION CALLED WHEN SAVING TEST TAKER'S INFORMATION INTO THE DATABASE
        Login = auth.sign_in_with_email_and_password(username, password) #AUTHENTICATING THE USER WITH THE USERNAME AND PASSWORD PROVIDED
                                                                         #THE RETURNED Login OBJECT CONTAINING AUTHENTICATION INFORMATION
        id_token =Login['localId'] #EXTRACTING THE localId FROM Login OBJECT
        user_id=id_token #THE UNIQUE IDENTIFIER IS ASSIGNED TO user_id
        db=firebase.database() #CONNECTING TO FIIREBASE REALTIME DATABASE THE REFERENCE TO THE DATABASE BEING ASSIGNED TO db
        print(name_entry.get()) #PRINTING IN TERMINAL THE ENTERED NAME AT THE END OF THE TEST
        user_info = {
            'name': name_entry.get(),
            'Positivity_Percentage':pos_per ,
            'Negative_Percentage': neg_per,
            'Positivity_Expresion_Percentage':pos_ex_per,
            'Negative_Expresion_Percentage':neg_ex_per,
            'Compatibility':comp
        } #CREATING A DICTIONARY CONTAINING THE INFORMATION TO SEND TO FIREBASE
        db.child('users').child(user_id).push(user_info) #PUSHING user_info AS A NEW CHILD NODE UNDER users NODE IN FIREBASE


    def showResults():
        global name_entry,pos_per,neg_per,pos_ex_per,neg_ex_per,comp

        #THE APPLICATION WILL FINALLY DISPLAY 2 WINDOWS INDICATING THE GATHERED ANALYZED DATA
        #Results WINDOW, HAVING MORE IN DEPTH ANALYSIS ON BOTH THE TEXT AND THE PERSON'S FACIAL EXPRESSIONS
        #WHILE ALSO BEING PROVIDED WITH THE Save Info BUTTON
        windowResults = tk.Tk()
        windowResults.title("Results:")
        windowResults.geometry('700x535')

        #AND Compatibility REVEALING THE OVERALL COMPUTED POSITIVITY AND NEGATIVITY SCORES
        #IN THE TEXT AND IN THE FACIAL MICROEXPRESSIONS
        #AND THEIR RESULTING COMPUTED COMPATIBILITY
        windowCompatibility = tk.Tk()
        windowCompatibility.title("Compatibility")
        windowCompatibility.geometry('470x200')

        mostusedwords, relevantverbs, mostpositivewords, mostnegativewords, positivityscoreoftext = sentimentAnalysis()
        #THE FIVE PARAMETERS REPRESENTING IN ORDER THE FIVE VARIABLES RETURNED BY THE sentimentAnalysis() METHOD:

        labelCompatibility = tk.Label(windowCompatibility, text="COMPUTED COMPATIBILITY:"+ str(
            100-(abs(round((computePositivity(positivityscoreoftext)-total_positivity_percentage),1)))) + "%")
        labelCompatibility.place(x=30, y=150)
        comp=100-(abs(round((computePositivity(positivityscoreoftext)-total_positivity_percentage),1)))
        #COMPUTING THE OVERALL COMPATIBILITY BY SUBTRACTING FROM THE MAXIMUM POSSIBLE VALUE THE MAGNITUDE OF
        #THE DIFFERENCE BETWEEN THE TWO PERCENTAGES OF POSITIVITY (IN TEXT AND FROM FACIAL EXPRESSIONS)

        labelTitleLeft = tk.Label(windowResults,
                                  text="IN DEPTH ANALYSIS ON TEXT:")
        labelTitleLeft.place(x=50, y=20)

        labelTitleRight = tk.Label(windowResults,
                                   text="FACIAL EMOTIONAL REACTION MONITORING:")
        labelTitleRight.place(x=300, y=20)

        labelparam1 = tk.Label(windowResults,
                               text="The most frequent words:")
        labelparam1.place(x=50, y=40)

        #DISPLAYING THE MOST USED WORDS IN THE TEXT
        param1scroll = scrolledtext.ScrolledText(windowResults,
                                                 wrap=tk.WORD,
                                                 width=18,
                                                 height=5,
                                                 font=("Times New Roman", 15))
        param1scroll.place(x=50, y=65)
        param1scroll.focus()
        param1scroll.insert(tk.INSERT, mostusedwords)

        #DISPLAYING THE MOST RELEVANT VERBS
        labelparam2 = tk.Label(windowResults,
                               text="The most relevant verbs:")
        labelparam2.place(x=50, y=185)
        param2scroll = scrolledtext.ScrolledText(windowResults,
                                                 wrap=tk.WORD,
                                                 width=18,
                                                 height=5,
                                                 font=("Times New Roman", 15))
        param2scroll.place(x=50, y=210)
        param2scroll.focus()
        param2scroll.insert(tk.INSERT, relevantverbs)

        #DISPLAYING THE WORDS WITH THE HIGHEST AFINN SCORE IN THE TEXT,
        #A scrolledtext WIDGET WAS CHOSEN DUE TO THE POSSIBILITY OF HAVING MANY WORDS WITH THE SAME HIGHEST SCORE
        labelparam3 = tk.Label(windowResults,
                               text="The most positive words:")
        labelparam3.place(x=50, y=330)
        param3scroll = scrolledtext.ScrolledText(windowResults,
                                                 wrap=tk.WORD,
                                                 width=18,
                                                 height=2,
                                                 font=("Times New Roman", 15))
        param3scroll.place(x=50, y=355)
        param3scroll.focus()
        param3scroll.insert(tk.INSERT, mostpositivewords)

        #DISPLAYING THE WORDS WITH THE LOWEST AFINN SCORE IN THE TEXT
        labelparam4 = tk.Label(windowResults, text="The most negative words:")
        labelparam4.place(x=50, y=410)
        param4scroll = scrolledtext.ScrolledText(windowResults, wrap=tk.WORD, width=18, height=2,
                                                 font=("Times New Roman", 15))
        param4scroll.place(x=50, y=435)
        param4scroll.focus()
        param4scroll.insert(tk.INSERT, mostnegativewords)

        #DISPLAYING TEXT'S OVERALL POSITIVITY, COMPUTING THE MEAN OF ALL AFINN SCORES OF THE TEXT'S WORDS
        #AND USING THE computePositivity() FUNCTION IN ORDER TO CONVERT THE AFINN SCORE IN PERCENTAGES
        labelparam5 = tk.Label(windowCompatibility,
                               text="TEXT'S POSITIVITY PERCENTAGE:" + str(
                                   computePositivity(positivityscoreoftext)) + "%")
        labelparam5.place(x=30, y=20)
        pos_per = str(computePositivity(positivityscoreoftext))

        #DISPLAYING TEXT'S OVERALL NEGATIVITY, COMPUTED BY SUBSTRACTING THE POSITIVITY PERCENTAGE FROM
        #THE TOTAL EMOTIONAL "PALETTE"
        labelparam5 = tk.Label(windowCompatibility,
                               text="TEXT'S NEGATIVITY PERCENTAGE:" + str(
                                   100 - computePositivity(positivityscoreoftext)) + "%")
        labelparam5.place(x=30, y=45)
        neg_per = str(100 - computePositivity(positivityscoreoftext))

        #DISPLAYING THE CUMULATIVE MEAN OF HAPPY
        #RECOMPUTED WITH EACH NEW FRAME,
        #DIVIDING THE SUM OF ALL HAPPY FACIAL EMOTIONS happy_sum BY THE count_monitoring VARIABLE
        labelmeanofhappy = tk.Label(windowResults,
                                    text="1. HAPPY FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                        round(mean_of_happy, 2)) + "%")
        labelmeanofhappy.place(x=300, y=70)

        labelmeanofsurprise = tk.Label(windowResults,
                                       text="2. SURPRISED FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                           round(mean_of_surprise, 2)) + "%")
        labelmeanofsurprise.place(x=300, y=110)

        labelmeanofsad = tk.Label(windowResults,
                                  text="3. SAD FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                      round(mean_of_sad, 2)) + "%")
        labelmeanofsad.place(x=300, y=150)

        labelmeanofangry = tk.Label(windowResults,
                                    text="4. ANGRY FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                        round(mean_of_angry, 2)) + "%")
        labelmeanofangry.place(x=300, y=190)

        labelmeanofdisgust = tk.Label(windowResults,
                                      text="5. DISGUSTED FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                          round(mean_of_disgust, 2)) + "%")
        labelmeanofdisgust.place(x=300, y=230)

        labelmeanoffear = tk.Label(windowResults,
                                   text="6. FEARFUL FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                       round(mean_of_fear, 2)) + "%")
        labelmeanoffear.place(x=300, y=270)

        labelmeanofneutral = tk.Label(windowResults,
                                      text="7. CALM FACIAL EXPRESSIONS' PERCENTAGE:" + str(
                                          round(mean_of_neutral, 2)) + "%")
        labelmeanofneutral.place(x=300, y=310)

        #DISPLAYING THE TOTAL POSITIVITY REVEALED FROM THE FACIAL EXPRESSIONS
        labeltotalfacialpoz = tk.Label(windowCompatibility,
                                       text="POSITIVE EXPRESSIONS' PERCENTAGE:" + str(
                                           round(total_positivity_percentage, 2)) + "%")
        labeltotalfacialpoz.place(x=30, y=80)
        pos_ex_per = str(round(total_positivity_percentage, 2))

        #DISPLAYING THE TOTAL NEGATIVITY REVEALED FROM THE FACIAL EXPRESSIONS
        labeltotalfacialneg = tk.Label(windowCompatibility,
                                       text="NEGATIVE EXPRESSIONS' PERCENTAGE:" + str(
                                           round(total_negativity_percentage, 2)) + "%")
        labeltotalfacialneg.place(x=30, y=105)
        neg_ex_per = str(round(total_negativity_percentage, 2))


        labelinsertname = tk.Label(windowResults,
                                   text="INSERT YOUR NAME:")
        labelinsertname.place(x=300, y=360)

        name_entry = tk.Entry(windowResults,width=45)
        name_entry.place(x=300, y=385)


        buttoninsertname = tk.Button(windowResults,
                                     text='Save Info',
                                     width=43,
                                     command=save_info)
        buttoninsertname.place(x=300, y=410)

        windowResults.mainloop()

    def insert_text():
        global textboxinserttext
        textboxinserttext = scrolledtext.ScrolledText(interfaceWindow, wrap=tk.WORD, width=65, height=13.5,
                                                      font=("Times New Roman", 15))
        textboxinserttext.place(x=50, y=225)
        textboxinserttext.focus()
        #THE FOCUS() FUNCTION MAKES SO THAT THE USER CAN INSTANTLY TYPE IN THE WORDS, WITHOUT THE NEED OF CLICKING ON THE textboxinserttext
        labeltextbox = tk.Label(interfaceWindow, text="Please insert your text below")
        labeltextbox.place(x=50, y=200)

    def stop_facial_emotion_monitoring(): #FUNCTION FOR STOPPING THE VIDEO TAKING
        global video
        video.release()

    def startCamera(): #ONCE STARTED THE FILMING,
                       #THE Start Monitoring BUTTON BECOMES Stop Monitoring, HAVING ASSIGNED TO IT
                       #THE stop_facial_emotion_monitoring FUNCTION WHEN PRESSED
        buttonstop = tk.Button(interfaceWindow,
                               text='Stop Monitoring',
                               width=50,
                               command=stop_facial_emotion_monitoring)
        buttonstop.place(x=50, y=95)


        global video #DECLARING THE GLOBAL VARIABLE video IN ORDER TO ALSO BE SEEN BY THE STOPPING FUNCTION
        happy_sum = np.float64(0.0) #VARIOUS VARIABLES' INITIALIZATION
        surprise_sum = np.float64(0.0)
        sad_sum = np.float64(0.0)
        angry_sum = np.float64(0.0)
        disgust_sum = np.float64(0.0)
        fear_sum = np.float64(0.0)
        neutral_sum = np.float64(0.0)
        #<emotion>_sum BEING THE SUMS OF ALL MONITORING ITERATIONS' RESULTS FOR ONE SPECIFIC <emotion>
        #IT IS IMPORTANT TO NOTICE THAT THE EMOTIONS ARE NOT SIMPLY OF float TYPE
        #DUE TO THEIR RETURNED TYPE FROM THE MONITORING
        video = cv2.VideoCapture(0)
        #ASSIGNING TO video A VideoCapture OBJECT INDEXED 0 FROM THE WEBCAM
        success, frame = video.read()
        #read() RETURNS A TUPLE CONTAINING THE BOOLEAN success FLAG AND THE FRAME
        if not success:
            print("Failed to capture frame from video source.")
            return
        #IF THE PROGRAM FAILES TO CAPTURE THE FRAME, THE MESSAGE WILL BE PRINTED IN THE TERMINAL

        def update_frame(happy_sum, surprise_sum, sad_sum, angry_sum, disgust_sum, fear_sum, neutral_sum):
            #DEFINING THE update_frame FUNCTION, PROVIDED WITH PARAMETERS MEANT TO STORE
            #THE CUMULATIVE SUMS OF EVERY EMOTION'S REVEALED SCORE
            global count_monitoring, mean_of_happy, mean_of_surprise, mean_of_sad, mean_of_angry, mean_of_disgust, mean_of_fear, mean_of_neutral, total_positivity_percentage, total_negativity_percentage
            #GLOBAL DECLARATION OF THE RESULTS BEING THEN DISPLAYED FROM THE showResults FUNCTION
            #count_monitoring IS USED FOR DESPLAYING IN TERMINAL EACH MONITORING ITERATION IN REAL-TIME,
            #BUT ALSO FOR THE MEANS' COMPUTATIONS
            nonlocal frame
            #THE frame VARIABLE IS NOT LOCAL TO THE update_frame FUNCTION
            success, frame = video.read()
            #USING THE video.read() METHOD, THE RESULT WILL BE STORED IN THE success VARIABLE,
            #WHILE THE ACTUAL TAKEN FRAME WILL BE STORED IN frame
            if success: #IF success IS True
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                #STORING THE GRAYSCALE OF THE FRAME IN THE gray VARIABLE
                #THE FRAME WAS CONVERTED FROM THE BGR COLOR SPACE IN ORDER TO BE ANALYZED
                resized_gray = cv2.resize(gray, (0, 0), fx=0.5, fy=0.5)
                #THE GRAYSCALE FRAME'S RESOLUTION BEING REDUCED BY 50% USING THE cv2.resize() METHOD
                #IN ORDER TO FACILITATE AND INCREASE THE SPEED OF IMAGE PROCESSING
                #PROCESSING SPEED BEING PARTICULARLY IMPORTANT DUE TO THE REAL-TIME NATURE OF THE EMOTION ANALYSIS
                faces = face_cascade.detectMultiScale(resized_gray, scaleFactor=1.1, minNeighbors=5)
                #STORING IN THE faces VARIABLE THE FACES DETECTED WITH detectMultiScale()
                #THE DETECTION IS BEING MADE ON THE RESIZED GRAYSCALE OF EACH FRAME
                for (x, y, w, h) in faces:
                    x, y, w, h = x * 2, y * 2, w * 2, h * 2
                    #THE COORDINATES ARE BEING SCALED UP TO THE FRAME'S ORIGINAL SIZE
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 1)
                    #A RED RECTANGLE WILL BE DRAWN IN REAL-TIME ON THE ORIGINAL FRAME
                    #IN ORDER TO SIGNAL FACE DETECTION TO THE USER

                    try: #ERROR MANAGEMENT
                        analyze = DeepFace.analyze(frame, actions=['emotion'], silent=True)
                        #THE analyze FUNCTION FROM THE DeepFace LIBRARY IS USED TO ANALYZE A FRAME IN ORDER TO REVEAL
                        #THE PORTRAIED EMOTION IN IT
                        #THE frame PARAMETER INDICATES THE FRAME TO BE ANALYZED, WHILE actions SPECIFICLY
                        #INDICATES THAT ONLY EMOTIONAL MONITORING IS TO BE PERFORMED,
                        #DeepFace HAVING ALSO OTHER CAPABILITIES OF ANALYSIS
                        #THE silent PARAM INDICATES THAT THE FUNCTION WILL NOT SHOW ANY OUTPUT IN THE CONSOLE

                        count_monitoring = count_monitoring + 1
                        #RECORDING THE NUMBER OF FACIAL EMOTION MONITORINGS IN ONE VIDEO TAKE
                        print("COUNTING MONITORINGS:")
                        print(count_monitoring)
                        #PRINTING THE MONITORINGS IN THE TERMINAL

                        happy_score = analyze[0]["emotion"]["happy"]
                        #ASSIGNING TO happy_score ONLY THE HAPPINESS REVEALED
                        #THE ANALYSIS PERFORMED BY DeepFace WILL QUANTIFY 7 EMOTIONS IN PERCENTAGES FOR EACH FRAME
                        happy_sum = np.sum([happy_sum, happy_score])
                        #COMPUTING THE TOTAL SUM OF ALL "HAPPY" PERCENTAGES
                        mean_of_happy = happy_sum / count_monitoring
                        #COMPUTING THE MEAN OF ALL CUMULATED "HAPPY" PERCENTAGES

                        #COMPUTING THE MEAN OF "SURPRISE" CUMULATIVELY
                        surprise_score = analyze[0]["emotion"]["surprise"]
                        surprise_sum = np.sum([surprise_sum, surprise_score])
                        mean_of_surprise = surprise_sum / count_monitoring

                        #COMPUTING THE MEAN OF "SAD" CUMULATIVELY
                        sad_score = analyze[0]["emotion"]["sad"]
                        sad_sum = np.sum([sad_sum, sad_score])
                        mean_of_sad = sad_sum / count_monitoring

                        #COMPUTING THE MEAN OF "ANGRY" CUMULATIVELY
                        angry_score = analyze[0]["emotion"]["angry"]
                        angry_sum = np.sum([angry_sum, angry_score])
                        mean_of_angry = angry_sum / count_monitoring

                        #COMPUTING THE MEAN OF "DISGUST" CUMULATIVELY
                        disgust_score = analyze[0]["emotion"]["disgust"]
                        disgust_sum = np.sum([disgust_sum, disgust_score])
                        mean_of_disgust = disgust_sum / count_monitoring

                        #COMPUTING THE MEAN OF "FEAR" CUMULATIVELY
                        fear_score = analyze[0]["emotion"]["fear"]
                        fear_sum = np.sum([fear_sum, fear_score])
                        mean_of_fear = fear_sum / count_monitoring

                        #COMPUTING THE MEAN OF "NEUTRAL" CUMULATIVELY
                        neutral_score = analyze[0]["emotion"]["neutral"]
                        neutral_sum = np.sum([neutral_sum, neutral_score])
                        mean_of_neutral = neutral_sum / count_monitoring

                        #COMPUTING AND PRINTING IN TERMINAL TOTAL POSITIVITY PERCENTAGE REVEALED FROM FACIAL MICROEXPRESSIONS:
                        total_positivity_percentage = mean_of_happy + mean_of_surprise + (mean_of_neutral / 2)
                        print("TOTAL POSITIVITY PERCENTAGE")
                        print(total_positivity_percentage)

                        #COMPUTING AND PRINTING IN TERMINAL TOTAL NEGATIVITY PERCENTAGE REVEALED FROM FACIAL MICROEXPRESSIONS:
                        total_negativity_percentage = mean_of_sad + mean_of_fear + mean_of_disgust + mean_of_angry + (
                                mean_of_neutral / 2)
                        print("TOTAL NEGATIVITY PERCENTAGE")
                        print(total_negativity_percentage)
                        print("\n")


                    except: #ERROR MANAGEMENT
                        print("No face detected.")

                img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
                #CONVERTING THE FRAME FROM BGR COLOR SPACE TO RGB COLOR SPACE WITH cvtColor
                #AND CONVERTING THE FRAME IN A Python Image Library IMAGE FOR FURTHER IMAGE PROCESSING
                img = img.resize((200, 200))  #RESIZING THE IMAGE TO 200 PIXELS IN WIDTH AND IN HEIGHT
                photo = ImageTk.PhotoImage(image=img) #CREATING A PHOTO IMAGE OBJECT DISPLAYABLE IN TKINTER GUI
                labelcamera.config(image=photo) #CONFIGURING THE labelcamera TKINTER WIDGET TO DISPLAY photo
                labelcamera.image = photo #MAKING THE photo REMAIN VISIBLE IN THE GUI
            labelcamera.after(15,
                              lambda: update_frame(happy_sum,
                                                   surprise_sum,
                                                   sad_sum,
                                                   angry_sum,
                                                   disgust_sum,
                                                   fear_sum,
                                                   neutral_sum))
            #THE update_frame FUNCTION IS CALLED AFTER A DELAY OF 15 MILISECONDS
            #after() METHOD OF labelcamera WIDGET WAS USED FOR SCHEDGULING

        update_frame(happy_sum, surprise_sum, sad_sum, angry_sum, disgust_sum, fear_sum, neutral_sum)


    button = tk.Button(interfaceWindow,
                       text='Show Results',
                       width=50,
                       command=showResults)
    button.place(x=50, y=130)

    buttonstart = tk.Button(interfaceWindow,
                            text='Start Monitoring',
                            width=50,
                            command=startCamera)
    buttonstart.place(x=50, y=95)

    labelcamera = tk.Label(interfaceWindow)
    labelcamera.place(x=500, y=10)

    buttoninsert = tk.Button(interfaceWindow,
                             text='Insert Text',
                             width=50,
                             command=insert_text)
    buttoninsert.place(x=50, y=60)

    labeltextbox = tk.Label(interfaceWindow,
                            text=username)
    labeltextbox.place(x=75, y=15)

    logout_button = tk.Button(interfaceWindow,
                              text='Logout',
                              width=50,
                              command=logout)
    logout_button.place(x=50, y=165)

    interfaceWindow.mainloop()
